package com.shengyan.pojo;

import lombok.Data;

/**
 * @author create by lsy on 2021/12/20 11:42 上午
 */
@Data
public class User {
    private Integer id;
    private String name;
    private String password;

}
