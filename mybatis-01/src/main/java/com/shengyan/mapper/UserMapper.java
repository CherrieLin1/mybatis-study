package com.shengyan.mapper;

import com.shengyan.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author create by lsy on 2021/12/20 11:46 上午
 */
public interface UserMapper {

    List<User> selectAll();

    List<User> selectByName(String name);

    User selectById(Integer id);

    int deleteById(Integer id);

    int insert(User user);

    /**
     * 分页查询1
     * @param startIndex  起始位置，代码从0开始，但是数据库的数据从1开始
     * @param pageSize 查询的数量
     * 例子（1，2）查询id为2，3两条数据
     * @return
     */
    List<User> pageQuery(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize);


    /**
     * 分页查询2  这么写的化，其实和上面是一样的，只是这时候默认起始位置为0
     * @param pageSize
     * @return
     */
    List<User> pageQuery1(@Param("pageSize") int pageSize);
}
