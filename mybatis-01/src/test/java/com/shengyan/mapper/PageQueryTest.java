package com.shengyan.mapper;

import com.shengyan.pojo.User;
import com.shengyan.utils.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author create by lsy on 2021/12/21 4:51 下午
 */
public class PageQueryTest {
    @Test
    public void pageQuery(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = mapper.pageQuery(2, 2);
        for (User user : users) {
            System.out.println("user = " + user);
        }
    }

    @Test
    public void pageQuery1(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = mapper.pageQuery1(3);
        for (User user : users) {
            System.out.println("user = " + user);
        }
    }
}
