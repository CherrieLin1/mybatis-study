package com.shengyan.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author create by lsy on 2021/12/22 4:15 下午
 */
@Data
public class Teacher {
    private int id;
    private String name;
    private List<Student> students;
}
