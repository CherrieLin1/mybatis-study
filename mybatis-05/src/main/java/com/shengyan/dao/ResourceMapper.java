package com.shengyan.dao;

import com.shengyan.pojo.Resource;

import java.util.List;
import java.util.Map;

/**
 * @author create by lsy on 2021/12/23 1:45 下午
 */
public interface ResourceMapper {
    /**
     * 动态sql choose ... when    当多个when的时候，只要第一个有，后面的when便无效，所有when都不满足的时候走otherWise
     * @param resource
     * @return
     */
    List<Resource> selectDynamic(Resource resource);

    /**
     * 动态sql2
     * @param resource
     * @return
     */
    List<Resource> selectDynamic1(Resource resource);

    /**
     * 公共语句提取
     * @param resource
     * @return
     */
    List<Resource> refidParam(Resource resource);

    /**
     * 万能的map  map中即可以是key,value  也可以是集合
     * @param map
     * @return
     */
    List<Resource> getByIds(Map<String,Object> map);


    /**
     * 万能的map 测试map
     * @param map
     * @return
     */
    List<Resource> getByNameAndPromotionType(Map<String,Object> map);
}
