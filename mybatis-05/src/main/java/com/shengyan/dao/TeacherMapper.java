package com.shengyan.dao;

import com.shengyan.pojo.Teacher;

/**
 * @author create by lsy on 2021/12/22 4:13 下午
 */
public interface TeacherMapper {

    Teacher getTeacherById1(int id);

    Teacher getTeacherById2(int id);

}
