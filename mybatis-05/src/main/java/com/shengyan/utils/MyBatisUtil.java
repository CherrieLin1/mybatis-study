package com.shengyan.utils;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 * @author create by lsy on 2021/12/22 4:17 下午
 */
public class MyBatisUtil {
    static SqlSessionFactory sqlSessionFactory;
    static {
        String resource = "mybatis-config.xml";
        try {
            InputStream resourceAsStream = Resources.getResourceAsStream(resource);
             sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 获取sqlSession   可以对比与mysql中的prepareStatement学习
     * @return
     */
    public static SqlSession getSession(){
        return sqlSessionFactory.openSession();
    }

}
