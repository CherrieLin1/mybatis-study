package com.shengyan.dao;

import com.shengyan.pojo.Resource;
import com.shengyan.utils.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author create by lsy on 2021/12/23 1:50 下午
 */
public class ResourceTest {


    @Test
    public void testDynamic(){
        SqlSession session = MyBatisUtil.getSession();
        ResourceMapper resourceMapper = session.getMapper(ResourceMapper.class);
        Resource resourceDO = new Resource();
        resourceDO.setName("燕子");
        resourceDO.setPromotionType(1);
        List<Resource> resourceDOS = resourceMapper.selectDynamic(resourceDO);
        for (Resource aDo : resourceDOS) {
            System.out.println("aDo = " + aDo);
        }
        session.close();
    }

    @Test
    public void testRefidParam(){
        SqlSession session = MyBatisUtil.getSession();
        ResourceMapper resourceMapper = session.getMapper(ResourceMapper.class);
        Resource resourceDO = new Resource();
        resourceDO.setName("燕子");
        resourceDO.setPromotionType(2);
        List<Resource> resourceDOS = resourceMapper.refidParam(resourceDO);
        for (Resource aDo : resourceDOS) {
            System.out.println("aDo = " + aDo);
        }
        session.close();

    }

    @Test
    public void testByIds(){
        SqlSession session = MyBatisUtil.getSession();
        ResourceMapper resourceMapper = session.getMapper(ResourceMapper.class);
        Map<String,Object> map = new HashMap<>();
        List<Long> list = new ArrayList<>(3);
        list.add(27047163L);
        list.add(27047164L);
        list.add(27047165L);
        map.put("ids",list);
        List<Resource> byIds = resourceMapper.getByIds(map);
        for (Resource byId : byIds) {
            System.out.println("byId = " + byId);
        }
    }


    @Test
    public void getByNameAndPromotionType(){
        SqlSession session = MyBatisUtil.getSession();
        ResourceMapper resourceMapper = session.getMapper(ResourceMapper.class);
        Map<String,Object> map = new HashMap<>();
        map.put("name","燕子");
        map.put("promotionType",2);
        List<Resource> byIds = resourceMapper.getByNameAndPromotionType(map);
        for (Resource byId : byIds) {
            System.out.println("byId = " + byId);
        }
    }
}
