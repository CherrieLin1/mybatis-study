package com.shengyan.dao;

import com.shengyan.pojo.Teacher;
import com.shengyan.utils.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

/**
 * @author create by lsy on 2021/12/22 4:23 下午
 */
public class TeacherTest2 {
    public static void main(String[] args) {
        SqlSession session = MyBatisUtil.getSession();
        TeacherMapper mapper = session.getMapper(TeacherMapper.class);
        Teacher teacherById1 = mapper.getTeacherById2(1);
        System.out.println("teacherById1 = " + teacherById1);
    }
}
