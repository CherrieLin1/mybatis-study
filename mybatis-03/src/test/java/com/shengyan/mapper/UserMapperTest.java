package com.shengyan.mapper;


import com.shengyan.dao.UserMapper;
import com.shengyan.pojo.User;
import com.shengyan.utils.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @author create by lsy on 2021/12/20 11:50 上午
 */
public class UserMapperTest {

    @Test
    public void testQuery() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
//        System.out.println("=======selectAll=======");
//        //查询全部用户
//        List<User> users = userMapper.selectAll();
//        for (User user : users) {
//            System.out.println(user);
//        }


//        System.out.println("========selectByName======");
//        List<User> gold = userMapper.selectByName("神");
//        for (User user : gold) {
//            System.out.println(user);
//        }


//        System.out.println("=======selectById=======");
//        User user = userMapper.selectById(3);
//        System.out.println(user);


        System.out.println("=======insert=======");
        User newUser = new User();
        newUser.setName("燕子");
        newUser.setPassword("123456");
        int insert = userMapper.insert(newUser);
        if (insert > 0){
            System.out.println("insert 成功");
        }
        System.out.println("newUser.getId() = " + newUser.getId());
        sqlSession.commit();


//        System.out.println("=======deleteById=======");
//        int i = userMapper.deleteById(8);
//        System.out.println("i = " + i);
//        if (i > 0){
//            System.out.println("删除ID=6的对象成功");
//        }
//        sqlSession.commit();



        sqlSession.close();

    }
}
