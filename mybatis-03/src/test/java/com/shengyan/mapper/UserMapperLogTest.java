package com.shengyan.mapper;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * @author create by lsy on 2021/12/20 7:50 下午
 */
public class UserMapperLogTest {
    static Logger logger = Logger.getLogger(UserMapperLogTest.class);

    @Test
    public void selectUser(){
        logger.debug("test debug !");
        logger.info("test info !");
        logger.error("test error!");
    }
}
