package com.shengyan.dao;

import com.shengyan.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author create by lsy on 2021/12/20 11:46 上午
 */
public interface UserMapper {

    @Select("select * from user")
    List<User> selectAll();

    @Select("select * from user where name like CONCAT('%', #{name}, '%')")
    List<User> selectByName(String name);

    @Select("select * from user where id = #{id}")
    User selectById(Integer id);

    @Delete("delete from user where id = #{id}")
    int deleteById(Integer id);

    @Insert("insert into user (name,pwd) values (#{name},#{password})")
    int insert(User user);
}
