package com.shengyan;

import com.shengyan.dao.StudentMapper;
import com.shengyan.pojo.Student;
import com.shengyan.utils.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @author create by lsy on 2021/12/22 3:43 下午
 */
public class StudentTest1 {
    public static void main(String[] args) {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> students = mapper.selectStudent1();
        for (Student student : students) {
            System.out.println("student = " + student);
        }
        sqlSession.close();

    }
}
