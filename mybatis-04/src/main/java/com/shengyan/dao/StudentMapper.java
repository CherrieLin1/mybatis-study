package com.shengyan.dao;

import com.shengyan.pojo.Student;

import java.util.List;

/**
 * @author create by lsy on 2021/12/21 7:44 下午
 */
public interface StudentMapper {

    List<Student> selectStudent1();

    List<Student> selectStudent2();


}
