package com.shengyan.pojo;

import lombok.Data;

/**
 * @author create by lsy on 2021/12/21 7:44 下午
 */
@Data
public class Student {
    private int id;
    private String name;
    private Teacher teacher;


    public static void main(String[] args) {
        Student student = new Student();
        student.setId(1);
        student.setName("yanzi");
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setName("秦老师");
        student.setTeacher(teacher);
        System.out.println("student = " + student);
    }
}
