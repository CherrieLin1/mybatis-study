package com.shengyan.pojo;

import lombok.Data;

/**
 * @author create by lsy on 2021/12/21 7:44 下午
 */
@Data
public class Teacher {
    private int id;
    private String name;
}
