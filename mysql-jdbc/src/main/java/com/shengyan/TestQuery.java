package com.shengyan;

import com.shengyan.utils.JdbcUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author create by lsy on 2021/12/14 5:24 下午
 */
public class TestQuery {
    public static void main(String[] args) {
        ResultSet resultSet = null;
        Statement statement = null;
        Connection connection = null;


        try {
            String sql = "select * from users";
            //获取链接
            connection = JdbcUtils.getConnection();

            //
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                System.out.println("resultSet.getObject(\"id\") = " + resultSet.getObject("id"));
                System.out.println("resultSet.getObject(\"password\") = " + resultSet.getObject("password"));
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭链接
            JdbcUtils.closeConnection(resultSet, statement, connection);
        }
    }

}
