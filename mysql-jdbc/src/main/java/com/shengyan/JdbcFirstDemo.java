package com.shengyan;


import com.mysql.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author create by lsy on 2021/12/14 4:07 下午
 */
public class JdbcFirstDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //url
        String url = "jdbc:mysql://localhost:3306/jdbcStudy?useUnicode=true&characterEncoding=utf8";
        //用户名
        String userName = "root";
        //密码
        String password = "123456";

        //1.加载驱动
//        不推荐使用这种方式,因为使用这种方式需要import该类，否则会报错，和驱动强耦合。切换到其他数据库也会有麻烦
//        DriverManager.registerDriver(new Driver());
        //加载这个类,推荐使用这个方式
        Class.forName("com.mysql.jdbc.Driver");


        //2.获取与数据库的链接
        Connection connection = DriverManager.getConnection(url, userName, password);

        //3.获取用于向数据库发送sql语句的statement
        Statement statement = connection.createStatement();



        //（1）查询
        String sql = "select * from users";
        //向数据库发sql,并获取代表结果集的resultset
        ResultSet resultSet = statement.executeQuery(sql);
        //取出结果集的数据
        while (resultSet.next()){
            System.out.println("id = " + resultSet.getObject("id"));
            System.out.println("name = " + resultSet.getObject("name"));
            System.out.println("password = " + resultSet.getObject("password"));
            System.out.println("email = " + resultSet.getObject("email"));
            System.out.println("birthday = " + resultSet.getObject("birthday"));
            System.out.println();
        }



        //（2）插入
        String insertSql = "INSERT INTO `users`(`NAME`, `PASSWORD`, `email`, `birthday`) VALUES ( '生燕', '123456', 'shengyan@sina.com', '1991-12-04')";
        int insertNum = statement.executeUpdate(insertSql);
        if (insertNum > 0){
            System.out.println("插入成功！");
        }

        //（3）修改
        String updateSql = "update users set name = 'shengyan1' where id = 5";
        int updateNum = statement.executeUpdate(updateSql);
        if (updateNum > 0){
            System.out.println("修改成功");
        }


        //（4）删除
        String deleteSql = "delete from users where id = 5";
        int deleteNum = statement.executeUpdate(deleteSql);
        if (deleteNum>0) {
            System.out.println("删除成功！");
        }


        //关闭连接，释放资源
        resultSet.close();
        statement.close();
        connection.close();

    }

}
