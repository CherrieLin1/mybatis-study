package com.shengyan;

import com.shengyan.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 可以预防sql注入
 *
 * @author create by lsy on 2021/12/14 5:46 下午
 */
public class PrepareStatementTest {
    public static void main(String[] args) {
        String userName = "' or '1=1";
        String password = "123456";
        //statement容易造成sql注入
//        login(userName, password);
        //prepareStatement防止sql注入
        prepareStatementLogin(userName, password);
    }


    /**
     * 普通编译
     * @param name
     * @param pwd
     */
    public static void login(String name, String pwd) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        String sql = "select * from users where name = ' " + name + "' and password ='" + pwd + "'";
        try {
            connection = JdbcUtils.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                System.out.println(resultSet.getObject("id"));
                System.out.println("登录成功！");
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            JdbcUtils.closeConnection(resultSet,statement,connection);
        }

    }


    /**
     * 预编译
     * @param userName
     * @param password
     */
    public static void prepareStatementLogin(String userName, String password){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = JdbcUtils.getConnection();
            //预编译
            String sql = "select * from users where name=? and password=?";

            preparedStatement = connection.prepareStatement(sql);


            //prepareStatement下标从1开始
            preparedStatement.setString(1,userName);
            preparedStatement.setString(2,password);


            //执行的时候sql不需要再写入一次了 resultSet = preparedStatement.executeQuery(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getObject("id"));
                System.out.println("登录成功！");
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            JdbcUtils.closeConnection(resultSet,preparedStatement,connection);
        }

    }

}
