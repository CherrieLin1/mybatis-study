package com.shengyan.utils;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * jdbc工具类
 * @author create by lsy on 2021/12/14 4:54 下午
 */
public class JdbcUtils {
    private static String name = null;
    private static String password = null;
    private static String url = null;
    private static String drive = null;

    static {
        try {
            InputStream resourceAsStream = JdbcUtils.class.getClassLoader().getResourceAsStream("db.properties");
            Properties prop = new Properties();
            prop.load(resourceAsStream);
            name = prop.getProperty("name");
            password = prop.getProperty("password");
            url = prop.getProperty("url");
            drive = prop.getProperty("drive");
            //加载数据库驱动
            Class.forName(drive);
        }catch (IOException | ClassNotFoundException e)  {
            e.printStackTrace();
        }
    }


    /**
     * 获取链接,一个请求申请一个链接
     * @return
     */
    public static Connection getConnection(){
        Connection connection =null;
        try {
             connection = DriverManager.getConnection(url, name, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }


    /**
     * 关闭链接
     * 一个链接同一时间只能执行一个sql
     * @param resultSet
     * @param statement
     * @param connection
     */
    public static void closeConnection(ResultSet resultSet, Statement statement,Connection connection){
        if (null != resultSet){
            try {
                resultSet.close();
                System.out.println("结果集关闭成功！");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if (null != statement){
            try {
                statement.close();
                System.out.println("statement关闭成功！");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if (null!=connection){
            try {
                connection.close();
                System.out.println("connection关闭成功！");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
